from random import randint 

player_name = input("Hi! What is your name?\n")

for attempt_num in range(1, 6):
    month = randint(1, 12)
    year = randint(1924, 2004)
    print("Guess", attempt_num, player_name, "were you born in ", month, "/", year, "?")
    
    answer = input("yes or no? ")
    
    if answer == "yes":
        print("I knew it!")
        exit()
    elif attempt_num == 5:
        print("I have other things to do. Good bye.")
    else:
        print("Drat! Lemme try again!")



